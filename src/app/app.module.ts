import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { DbService } from './services/db.service';
import {
  dataStoreFactory,
  DATA_STORE_TOKEN,
} from './auth/data-store/data-store.factory';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule],
  providers: [
    DbService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: DATA_STORE_TOKEN, useFactory: dataStoreFactory },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
