import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { DbService } from '../services/db.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  constructor(private readonly dbService: DbService) {}

  name = new FormControl('');
  id: string = '';
  mode: string = 'Create Note';
  modal: boolean = false;
  data: any = [];

  ngOnInit() {
    this.listAll();
  }

  listAll() {
    this.dbService.find().then((data) => {
      this.data = data;
    });
  }

  delete(_id) {
    this.dbService.remove({ _id }).then((err) => {
      console.log(err);
      this.listAll();
    });
  }

  submit(data?) {
    if (this.mode === 'Create Note') {
      this.dbService
        .save({
          field: 'Name',
          value: this.name.value,
          date: new Date(),
        })
        .then((data) => {
          this.listAll();
          this.modal = false;
        });
    } else {
      this.dbService.update({ value: this.name.value }, this.id).then((res) => {
        this.listAll();
        this.modal = false;
      });
    }
  }

  openModal(data?) {
    this.modal = true;
    this.name.setValue('');
    if (data) {
      this.id = data._id;
      this.name.setValue(data.value);
      this.mode = 'Edit Note';
    }
  }

  closeModal() {
    this.modal = false;
  }
}
