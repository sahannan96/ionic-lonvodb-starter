import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'ionic-linvodb',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
